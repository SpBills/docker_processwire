FROM php:5.6-apache

RUN    apt-get update \
&& apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libmcrypt-dev libpng-dev zziplib-bin \
&& a2enmod rewrite \
&& a2enmod ssl \
&& docker-php-ext-install mysqli pdo_mysql iconv mcrypt zip \
&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
&& docker-php-ext-install gd 

EXPOSE 80
EXPOSE 443

# Remove all default site files in /var/www/html
RUN rm -rf /var/www/html/*

# Copy ProcessWire core files
COPY --chown=www-data:www-data ProcessWire/ /var/www/html/

VOLUME /var/www/html