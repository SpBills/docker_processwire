# Instructions
First, clone this repository.

## Using a premade Docker container


ProcessWire version: 3.0.148


`docker pull registry.gitlab.com/spbills/docker_processwire/processwire_web:latest`

**Must use your own database if you don't have an existing one.**


See below for k8s deployment.


## Building your own Docker container


Then, make sure to recursively chown the ProcessWire directory if you are building a docker container.


`chown -R www-data:www-data ProcessWire`


### IF YOU NEED A NEW DATABASE:


Then, run the docker-compose file. `docker-compose up docker-compose.yml`

This will create a blank MariaDB database container, and the web server. Make sure to edit the docker-compose to supply a volume if you don't want it to reset every time.


### IF YOU ALREADY HAVE A DATABASE:


Then, just do `docker build .` and it will build the docker container. It does not do setup, so you will have to provide it with your own Volume whenever you run it so it doesn't delete every time.


## Putting an existing container on Kubernetes.


I have created a premade k8s boilerplate for use. It allocates 1Gi of RAM and 1 vCPU (extreme overkill unless you're going over 100k per day).


It pulls from this repository's pre-built Docker container.


Make sure to edit the k8s.yaml file to supply a persisent volume if you don't want it to reset every time. `Deployment`s don't seem like a great option, as ProcessWire doesn't horizontally scale well.